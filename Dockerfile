FROM ubuntu:16.04

LABEL maintainer="allconnect."

RUN apt-get update && apt-get install -y --no-install-recommends \
        apache2 \
        software-properties-common \
        supervisor \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*

RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php

RUN apt-get update && apt-get install -y --no-install-recommends \
        libapache2-mod-php5.6 \
        php5.6 \
        php5.6-cli \
        php5.6-curl \
        php5.6-dev \
        php5.6-gd \
        php5.6-imap \
        php5.6-mbstring \
        php5.6-mcrypt \
        php5.6-mysql \
        php5.6-pgsql \
        php5.6-pspell \
        php5.6-xml \
        php5.6-xmlrpc \
        php5.6-zip \
        php-apcu \
        php-memcached \
        php-pear \
        php-redis \
        vim \
        curl \
        zip \
        unzip \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*


RUN a2enmod rewrite ssl headers

COPY conf/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY conf/default-ssl.conf /etc/apache2/sites-enabled/default-ssl.conf
COPY conf/security.conf /etc/apache2/conf-enabled/security.conf

COPY key/server.crt /etc/ssl/certs/ssl-cert-snakeoil.pem
COPY key/server.key /etc/ssl/private/ssl-cert-snakeoil.key

COPY conf/php70.ini /etc/php/7.0/apache2/php.ini

COPY conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY script/run.sh /run.sh
RUN chmod 755 /run.sh

COPY conf/config /config

EXPOSE 80 
EXPOSE 443
CMD ["/run.sh"]
