#!/bin/bash

#systemctl list-unit-files

yum install -y docker 
systemctl restart docker.service
systemctl enable docker.service

yum install -y git


# --- START UserData SECTION ---------
export ENV_REPO_DK1=https://hkd1123@bitbucket.org/hkd1123/docker-apache-php.git
export ENV_REPO_DK2=https://hkd1123@bitbucket.org/hkd1123/docker-apache-php-oci.git
export ENV_REPO_IF=design-ac@design-ac.git.backlog.com:/MOBANAVI/interface.git
export ENV_REPO_APP=design-ac@design-ac.git.backlog.com:/MOBANAVI/moba-navi.jp.git
export ENV_REPO_APPSP=design-ac@design-ac.git.backlog.com:/MOBANAVI/sp.moba-navi.jp.git

sshcfg=$(cat <<SSHC
Host design-ac.git.backlog.com
   HostName design-ac.git.backlog.com
   IdentityFile ~/.ssh/id_rsa_bb
   StrictHostKeyChecking no
SSHC
)
echo "$sshcfg" > /root/.ssh/config
chmod 600 /root/.ssh/config

sshcfg=$(cat <<SSHC
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAykTNtu8oBok4pEWd3Q0EkAwVPn4Mtcq/Iwf9wse9W58YoSOd
******************
WbExWo9inFHYLyb6qmxgbQKBgQDNY1PbOZCDs3cLiEi4fB8LBgoj3UhNAPDa1gN0
quQm5gJG5UjIW9XJvhuynrTeVGLxW5AJNW3IuWWtHOks/eZfS8Kmw8P6WmZw0XNX
nDqxbgvsEkcExVFnUYxJ13wlFhcKbk/3qBUV6j+pLtg0TOrHNKHhZwYDD3A+CdKC
NYjicQKBgQC2mTkRW54r3s2Rzfg/SAVgA1faE731/0AcPIBMBhdyLcM1x+toPVCl
vIoCv2blxgqHOMTkjainrkennP0KdCq+H839xfKbCZG9JI/clJMigCAszHYwicO3
+HYLlL8fUcrbEZQVXdCGN2AgXl/I2qd9FADFF5gQD06MvayOK0k+HQ==
-----END RSA PRIVATE KEY-----
SSHC
)
echo "$sshcfg" > /root/.ssh/id_rsa_bb
# chown ec2-user:ec2-user /home/ec2-user/.ssh/id_rsa_bb
chmod 600 /root/.ssh/id_rsa_bb

cd /root
mkdir mbApp

cd mbApp

git clone $ENV_REPO_DK1 dockerApp
git clone $ENV_REPO_DK2 dockerAppIF

git clone $ENV_REPO_IF AppIF
git clone $ENV_REPO_APP AppPC
git clone $ENV_REPO_APPSP AppSP

cd dockerApp
docker build -f Dockerfile -t mbapppc:latest .

cd ..
cd dockerAppIF
docker build -f Dockerfile -t mbappif:latest .

cd ..

docker network create mobalocal

docker run --net mobalocal -d -v `pwd`/AppPC:/var/www/html -p 80:80 -p 443:443 mbapppc:latest

docker run --net mobalocal --name mobaif -d -v `pwd`/AppIF/NewShop/phpmodules:/var/www/html/shopnavi/ -v `pwd`/AppIF/NewShop/phprequest:/var/www/html/shopgw/ -p 8080:80 -p 8443:443 mbappif:latest

cd AppPC
chmod -R 777 pc/fuel/app/cache
chmod -R 777 pc/fuel/app/logs
chmod -R 777 pc/fuel/app/tmp



# --- END UserData SECTION ---------